#========================================
# Bundle together the pygame stuff into what people call a game engine.
#
# Has a pygame.surface, and includes methods from pygame.draw, but allows
# performing an affine transformation on the coordinates first.
#
# Maintains its own list of active bounding boxes and a list of things that
# can be rendered.
#
# Contains the game's event loop, which renders the things that can be rendered
# and then checks for mouse events in its list of bounding boxes.

import pygame
import os
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (0,0)

class Color:
    black    = (   0,   0,   0)
    white    = ( 255, 255, 255)
    red      = ( 255,   0,   0)
    blue     = (   0,   0, 255)
    green    = (   0, 255,   0) 
    pink     = ( 255,  80,  80)
    gray     = (  80,  80,  80)

    highlight = green
    

# A thing which can be rendered.
# This class, right now, draws a little circle thingy.  But eventually it will be 
# a mixin.
class Renderable(object):
    def __init__(self, pos, active=True):
        self.pos = pos
        self.active = active
        self.default_color = Color.black
        self.color = self.default_color
        self.primed = False

    def enable_rendering(self, affine_draw_package):
        self.draw = affine_draw_package

    def render(self):
        self.draw.circle(self.color, self.pos, 3, 1)
        if self.active:
            bb = self.draw.circle(self.color, self.pos, 1, 0)
            return [bb]
        else:
            return []

    def handle_mouseevent(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            pass
        elif event.type == pygame.MOUSEMOTION: 
            self.drag(event.pos)
        elif event.type == pygame.MOUSEBUTTONUP:
            self.primed = False
            #print "Click!"
            self.click()

    def drag(self, pos):
        pass
        #print "drag" + str(pos)

    def click(self):
        pass
        
# This allows you to draw stuff on the monitor after performing an affine transformation.
class AffineDrawPackage:
    def __init__(self, screen, offset, scale):
        self.raw_screen = screen
        self.offset = offset
        self.scale = scale
        self.text_cache = {}
        self.font = pygame.font.Font(None, int(self.scale))

# input: abstract coords; output: window coords
    def transform(self, obj):
        if type(obj) == tuple and len(obj) == 2:
            return (int(obj[0] * self.scale + self.offset[0]), 
                    int(obj[1] * self.scale + self.offset[1]))
        elif type(obj) == list:
            return [self.transform(p) for p in obj]
        else:
            raise Exception("Don't know how to transform a %s: %s" % (str(type(obj)), str(obj)))

# input: window coords; output: abstract coords
    def untransform(self, obj):
        if type(obj) == tuple and len(obj) == 2:
            return (float(obj[0] - self.offset[0]) / self.scale , 
                    float(obj[1] - self.offset[1]) / self.scale )
        elif type(obj) == list:
            return [self.untransform(p) for p in obj]
        else:
            raise Exception("Don't know how to untransform a %s: %s" % (str(type(obj)), str(obj)))

    def circle(self, color, pos, radius, width=0):
        t_pos = self.transform(pos)
        return pygame.draw.circle(self.raw_screen, color, t_pos, int(self.scale*radius), width)

    def polygon(self, color, pointlist, width=0):
        t_pointlist = self.transform(pointlist)
        return pygame.draw.polygon(self.raw_screen, color, t_pointlist,  width)

    def line(self, color, start_pos, end_pos, width=1):
        t_start = self.transform(start_pos)
        t_end = self.transform(end_pos)
        return pygame.draw.line(self.raw_screen, color, t_start, t_end,  width)

    def lines(self, color, closed, pointlist, width=1):
        t_pointlist = self.transform(pointlist)
        return pygame.draw.lines(self.raw_screen, color, closed, t_pointlist, width)

    def text(self, characters, pos, color):
        if not(characters in self.text_cache):
            self.text_cache[characters] = self.font.render(characters, True, color, Color.white) # a surface
        text_surface = self.text_cache[characters] 
        text_boundingbox = text_surface.get_rect(center = self.transform(pos))
        return self.raw_screen.blit(text_surface, text_boundingbox)

class GameEngine:
    def __init__(self, offset, scale):
        pygame.init()
        pygame.font.init()
        self.clock=pygame.time.Clock() 
        pygame.mouse.set_cursor(*pygame.cursors.broken_x)
        self.raw_screen = pygame.display.set_mode((948,1030), pygame.RESIZABLE) #work computer
        self.draw = AffineDrawPackage(self.raw_screen, offset, scale)
        self.click_radius = 2
        self.last_unique_id = 1000
        self.animation_time = 500 # milliseconds
        self.renderables = []
        self.animations = []
        self.bounding_boxes = []

    def flip(self):
        pygame.display.flip()

    def add(self, renderable):
        renderable.enable_rendering(self.draw)
        renderable.unique_id = self.last_unique_id
        self.last_unique_id += 1
        self.renderables.append(renderable)

    def add_animation(self, renderable):
        #print "Adding wire"
        renderable.enable_rendering(self.draw)
        renderable.run_animation = False
        renderable.unique_id = self.last_unique_id
        self.last_unique_id += 1
        self.animations.append(renderable)
    
    def remove(self, id_to_remove):
        for i in range(len(self.renderables)):
            if self.renderable[i].unique_id == id_to_remove:
                self.renderables.pop(i) 
    
    def main_loop(self):
        done = False
        dragging = False
        drag_handler = None
        animate_flag = False
        while done==False:
            for event in pygame.event.get(): # User did something
                if event.type == pygame.QUIT: # If user clicked close
                    done=True # Flag that we are done so we exit this loop
                if event.type == pygame.KEYUP:
                    pass # not sure what to do with keypresses yet
                if event.type == pygame.VIDEORESIZE:
                    pass
                    #print event
                if event.type == pygame.MOUSEBUTTONDOWN:
                    ul = (event.pos[0] - self.click_radius, event.pos[1] - self.click_radius)
                    event_region = pygame.Rect(ul , (2*self.click_radius,2*self.click_radius))
                    box_index = event_region.collidelist(self.bounding_boxes)
                    if(box_index != -1):
                        dragging = True
                        drag_handler = self.handlers[box_index]
                if (event.type == pygame.MOUSEMOTION) and dragging: 
                    drag_handler(event)
                if (event.type == pygame.MOUSEBUTTONUP) and dragging:
                    drag_handler(event)
                    dragging = False
                    drag_handler = None
                    animate_flag = True
            self.render()
            if animate_flag:
                self.animate()
                animate_flag = False

    def render(self):       
        self.bounding_boxes = []
        self.handlers = []
        self.raw_screen.fill(Color.white)
        for thingy in self.renderables:
            boxes = thingy.render()
            self.bounding_boxes.extend(boxes)
            self.handlers.extend([thingy.handle_mouseevent] * len(boxes))
        pygame.display.flip()
        # While just waiting for events, limit to 20 frames per second
        self.clock.tick(20)

    def animate(self):
        #print "animation routine started"
        active_animations = []
        for thingy in self.animations:
            #print type(thingy)
            if thingy.run_animation:
                #print "noticing wire animation request"
                active_animations.append(thingy)
                #print active_animations
        if len(active_animations) == 0:
            return
        #print "Animation!"
        t = self.clock.get_time()
        while(t < self.animation_time):
            for thingy in active_animations:
                thingy.animate(float(t)/self.animation_time)
            pygame.display.flip()
            self.clock.tick(60)
            t += self.clock.get_time()

    def quit(self):
        pygame.font.quit()
        pygame.quit()

