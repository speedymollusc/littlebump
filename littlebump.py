#!/usr/bin/env python

from game_engine import *
from reducedword import *
from typeb import *
import sys

if len(sys.argv) < 2:
    print "Usage: "
    print "./littlebump.py A [1,2,1,3,2,1,4,3,2,1]"
    print "./littlebump.py B [1,2,0,3,2,1,4,3,2,1]"
    exit()

engine = GameEngine((100,350),50)
if len(sys.argv) == 2:  
    crossings =[1,2,1,3,2,1,4,3,2,1] 
else:
    crossings = [int(x) for x in sys.argv[2:]]

if sys.argv[1] == "A":
    print "A"
    w = ReducedWord(crossings)

elif sys.argv[1] == "B":   
    print "B"
    w = ReducedWordTypeB(crossings)

engine.add(w)

for x in w.crossings:
    engine.add(x)

for wire in w.wires:
    engine.add(wire)
    engine.add_animation(wire)

engine.main_loop()
engine.quit()

