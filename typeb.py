from reducedword import *

class CrossingTypeB(Crossing):
    def __init__(self, pos, mirror = True):
        Crossing.__init__(self, pos)
        if mirror:
            self.buddy = CrossingTypeB((pos[0], -pos[1]), False)
            self.buddy.buddy = self

    def drag(self, drag_pos):
        Crossing.drag(self, drag_pos)
        self.buddy.offset = (0.0, -self.offset[1])

    def click(self):
        Crossing.click(self)
        Crossing.click(self.buddy)
        self.buddy.animate_my_wire = False

    def transpose(self, pi):
        i = self.pos[1]
        if(i==0):
            (pi[-1], pi[1]) = (pi[1],pi[-1])
        elif(i>0):
            (pi[i], pi[i+1]) = (pi[i+1], pi[i])  
            (pi[-i], pi[-i-1]) = (pi[-i-1], pi[-i])  
        return pi

class ReducedWordTypeB(ReducedWord):
    def __init__(self, w):
        self.setup_params()
        self.crossing_representatives = []
        for i in range(len(w)):
            x = CrossingTypeB((i,w[i])) 
            self.crossing_representatives.append(x)
        self.max_wire = max(w)
        self.min_wire = -self.max_wire
        self.flatten_crossings()
        self.update_combinatorics()

    def crossing_pos_from_heights(self,p1, p2):
        if (p1==-1 and p2 == 1):
            return 0
        elif p1 < 0:
            return max(p1, p2)
        else:
            return min(p1, p2)

    def compute_partial_perms(self):
        things_to_permute = self.things_to_permute()
        pi = dict(zip(things_to_permute, things_to_permute))
        self.partial_perms = [self.inv_perm(pi)]
        for x in self.crossing_representatives:
            pi = x.transpose(pi)
            self.partial_perms.append(self.inv_perm(pi))

    def flatten_crossings(self):
        self.crossings = []
        self.buddy_location = []
        current_representative = 0
        for i in range(len(self.crossing_representatives)):
            x = self.crossing_representatives[i]
            if x.pos[1] < 0:
                self.crossing_representatives[i] = x.buddy
                x = x.buddy

            i = len(self.crossings)
            self.crossings.append(x)
            self.crossings.append(x.buddy)
            self.buddy_location.extend([i+1, i])

    def update_combinatorics(self):
        self.min_wire = -self.max_wire
        self.flatten_crossings()
        #print "========================\n", [x.pos[1] for x in self.crossing_representatives ]
        ReducedWord.update_combinatorics(self)
        #print "Crossings:", [t.pos[1] for t in self.crossing_representatives]

    def render(self):
        self.update_combinatorics()
        boxes=[]
        pi = self.perm()
        length = len(self.crossing_representatives)
        for i in range(1, self.max_wire + 1):
            boxes.append(self.draw.text(str(i), (-1.5, i-0.5), self.color))
            boxes.append(self.draw.text(str(pi[i]), (length + 0.5, i-0.5), self.color))
            self.draw.line(self.color, (-1, i-0.5), (length, i-0.5), self.line_width)
        for i in range(-self.max_wire, 0):
            boxes.append(self.draw.text(str(i), (-1.5, i+0.5), self.color))
            boxes.append(self.draw.text(str(pi[i]), (length + 0.5, i+0.5), self.color))
            self.draw.line(self.color, (-1, i+0.5), (length, i+0.5), self.line_width)

        if self.is_reduced():
            self.draw.text("Reduced", (self.length/2, self.max_wire+1.0), self.color)
        else:
            self.draw.text("Not Reduced", (self.length/2, self.max_wire+1.0), Color.red)
        if self.bump_direction == 1:
            self.draw.text("Bumping Down", (self.length/2, self.max_wire+2.0), self.color)
        elif self.bump_direction == -1:
                self.draw.text("Bumping Up", (self.length/2, self.max_wire+2.0), self.color)
        return boxes

    def fix_nodes_for_rendering(self, nodes):
        shifted_nodes = []
        for (x,y) in nodes:
            if y < 0:
                shifted_nodes.append((x-0.5, y+0.5))
            elif y > 0:
                shifted_nodes.append((x-0.5, y-0.5))
        (first, last) = (shifted_nodes[0], shifted_nodes[-1])
        shifted_nodes.insert(0, (first[0]-0.5, first[1]))
        shifted_nodes.append((last[0]+0.5, last[1]))
        return shifted_nodes


    def things_to_permute(self):
        things = range(1, self.max_wire +1)
        things.extend(range(-self.max_wire, 0))
        return things

    def perm(self):
        things_to_permute = self.things_to_permute()
        pi = dict(zip(things_to_permute, things_to_permute))
        for x in self.crossing_representatives:   
            pi = x.transpose(pi)
        return pi

    def compute_partial_perms(self):
        things_to_permute = self.things_to_permute()
        pi = dict(zip(things_to_permute, things_to_permute))
        self.partial_perms = [self.inv_perm(pi)]
        for x in self.crossing_representatives:
            pi = x.transpose(pi)
            self.partial_perms.append(self.inv_perm(pi))

    def crossing_count(self):
        crossing_count = 0
        for x in self.crossing_representatives:
            if x.pos[1] == 0:
                crossing_count += 1
            else:
                crossing_count += 2
        return crossing_count

    def is_reduced(self):
        return self.inversions() == self.crossing_count()

    def find_wires(self, crossing):
        if crossing.pos[1] != 0:
            ReducedWord.find_wires(self, crossing)
        else:
            (x, y) = crossing.pos
            (pi, tau) = (self.inv_perm(self.partial_perms[x]), self.inv_perm(self.partial_perms[x+1]))
            points_moved = [pi[t] for t in self.things_to_permute() if pi[t] != tau[t]]
            node_lists = []
            valid_wires = []
            #print "Points_moved:", points_moved
            for p in points_moved:
                nodes = []
                for i in range(len(self.partial_perms)):
                    pi = self.partial_perms[i]
                    nodes.append((i, pi[p]))
                if abs(nodes[crossing.pos[0]][1] - crossing.pos[1]) <= 1:
                    node_lists.append(self.fix_nodes_for_rendering(nodes))           
                    valid_wires.append(nodes)
# find all places where wires cross
            self.wires_cross_at = []
            #print "Valid wires: ", valid_wires
            for i in range(1, len(valid_wires[0])):
                pre1 = valid_wires[0][i-1][1]
                pre2 = valid_wires[1][i-1][1]
                post1 = valid_wires[0][i][1]
                post2 = valid_wires[1][i][1]
                height_diff_pre = pre2 - pre1
                height_diff_post = post2 - post1
                if height_diff_pre == -height_diff_post:
                    yi = self.crossing_pos_from_heights(pre1, pre2)
                    self.wires_cross_at.append((i-1, 0))
            for (wire, nodes) in zip(self.wires, node_lists):
                wire.nodes = nodes
                #print crossing.pos
                wire.start_x = x
                wire.run_animation = True
            #print "Wires cross at:",  self.wires_cross_at


    def is_vmarked_nearly_reduced(self, i):
        j = int(i/2) # each crossing representative corresponds to 2 entries in crossing list
        c = self.crossing_representatives.pop(j)
        deleted_inversion_count = self.inversions()
        self.crossing_representatives.insert(j, c)
        self.flatten_crossings()
        if self.crossings[i].pos[1] == 0:
            deleted_crossing_count = self.crossing_count() - 1
        else:
            deleted_crossing_count = self.crossing_count() - 2
        #if self.crossings[i].pos[1] == 0:
            #print "First crossing vmarked:", self.crossings[i].pos, deleted_inversion_count, deleted_crossing_count
        return deleted_inversion_count == deleted_crossing_count
        
    
    def descents(self):
        pass
    
