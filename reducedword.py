#!/usr/bin/env python

from game_engine import *
from copy import *

class Crossing(Renderable):
    def __init__(self, pos, active=True):
        super(Crossing, self).__init__(pos, active)
        self.word = None
        self.dragging = False
        self.animate_my_wire = False
        self.width = 6
        self.offset = (0,0)
        self.arrow_color = Color.red
        self.clipping_epsilon = 0.04
        self.bump_direction = None
        self.max_move = 1.0
        self.min_move = -1.0

    def render(self):
        buttons = []
        pos = (self.pos[0] + self.offset[0], self.pos[1] + self.offset[1])

        ul = (pos[0] - 0.5, pos[1] - 0.5)
        ur = (pos[0] + 0.5, pos[1] - 0.5)
        ll = (pos[0] - 0.5, pos[1] + 0.5)
        lr = (pos[0] + 0.5, pos[1] + 0.5) 
        uc = (pos[0], pos[1] - 0.9)
        lc = (pos[0], pos[1] + 0.9)

        ep = self.clipping_epsilon
        self.draw.polygon(Color.white, [(ul[0]+ep,ul[1]-2*ep),uc,(ur[0]-ep,ul[1]-2*ep),
                                       (lr[0]-ep,lr[1]+2*ep),lc,(ll[0]+ep,ll[1]+2*ep)], 0)
                                        
        if self.active:
            #color = self.arrow_color
            arrow_color = Color.red
            arrowhead_width = 0.3
            arrowshaft_width = 0.15
            arrowshaft_length = 0.5
            arr_l = pos[0] - arrowhead_width
            arr_r = pos[0] + arrowhead_width
            shaft_l = pos[0] - arrowshaft_width
            shaft_r = pos[0] + arrowshaft_width
            arr_t = pos[1] - arrowshaft_length
            arr_b = pos[1] + arrowshaft_length
            self.draw.polygon(arrow_color, [(shaft_l, arr_t), (shaft_r, arr_t), (shaft_r, arr_b), (shaft_l,arr_b)], 0)
            if 1:
            #if self.min_move < -0.1:
                self.draw.polygon(arrow_color, [uc, (arr_l, arr_t), (arr_r, arr_t)], 0)
            if 1:
            #if self.max_move > 0.1:
                self.draw.polygon(arrow_color, [lc, (arr_l, arr_b), (arr_r, arr_b)], 0)

            #self.draw.circle(self.arrow_color, pos, 0.5)]
        self.draw.line(self.color, ul, lr, self.width)
        box = self.draw.line(self.color, ur, ll, self.width)
        if self.active:
            return [box]
            #return [pygame.Rect((-0.5+pos[0],-0.9+pos[1]),(0.5+pos[0], 0.9+pos[1]))]
        else:
            return []

    def drag(self, drag_pos):
        self.dragging = True
        drag_y = self.draw.untransform(drag_pos)[1] - self.pos[1];
        drag_y = min(drag_y, self.max_move)
        drag_y = max(drag_y, self.min_move)
        #print drag_y
        self.offset = (0.0, drag_y)

    def click(self):
        self.dragging = False
        if self.offset[1] < -0.3:
            self.pos = (self.pos[0], self.pos[1] - 1)
            self.bump_direction = -1
        elif self.offset[1] > 0.3:
            self.pos = (self.pos[0], self.pos[1] + 1)
            self.bump_direction = 1
        self.animate_my_wire = True
        self.offset = (0.0,0.0)

    def transpose(self,pi):
        i = self.pos[1]
        (pi[i], pi[i+1]) = (pi[i+1], pi[i])
        return pi

class Wire(Renderable):
    def __init__(self, color):
        self.active = False
        self.color = color

    def render(self):
        if self.active:
            self.draw.lines(self.color, False, self.nodes,7)
        return []

    def animate(self, t):
        x_offset = len(self.nodes) * t
        right_side = self.start_x + x_offset + 0.5
        left_side = self.start_x - x_offset  + 2.5
        for x in (right_side, left_side):
            min_x = int(x-0.5) 
            max_x = int(x+0.5)
            min_x = max(min(min_x, len(self.nodes)-1),0)
            max_x = max(min(max_x, len(self.nodes)-1),0)
            self.draw.line(self.color, self.nodes[min_x], self.nodes[max_x], 7)
        self.active = True
            
class ReducedWord(Renderable):
    def __init__(self, w):
        self.setup_params()
        for i in range(len(w)):
            x = Crossing((i,w[i])) 
            self.crossings.append(x)
        self.update_combinatorics()

    def setup_params(self):
        self.line_width = 4
        self.color = Color.black
        self.crossings = []
        self.wires = (Wire(Color.green), Wire(Color.blue))
        self.bump_direction = None
        self.bumped_crossing = None    
        self.other_crossing = None    

    def __len__(self):
        return len(self.crossings)

    def update_bump_constraints(self):
        for i in range(len(self.crossings)):
            if self.bump_direction == None:
                self.crossings[i].active = self.is_vmarked_nearly_reduced(i)
            else:
                pos = self.crossings[i].pos
                self.crossings[i].active = (pos in self.wires_cross_at)
            if i == self.bumped_crossing: # Make it so I can ONLY undo the crossing I just moved.
                self.crossings[i].arrow_color = Color.gray
                if self.bump_direction == 1:
                    self.crossings[i].min_move = -1.0
                    self.crossings[i].max_move = 0.0
                elif self.bump_direction == -1:
                    self.crossings[i].min_move = 0.0
                    self.crossings[i].max_move = 1.0
            elif self.bump_direction != None and self.crossings[i].pos in self.wires_cross_at: 
                self.crossings[i].arrow_color = Color.red
                if self.bump_direction == -1:
                    self.crossings[i].min_move = -1.0
                    self.crossings[i].max_move = 0.0
                elif self.bump_direction == 1:
                    self.crossings[i].min_move = 0.0
                    self.crossings[i].max_move = 1.0
            else:
                self.crossings[i].arrow_color = Color.red
                self.crossings[i].min_move = -1.0
                self.crossings[i].max_move = 1.0

    def update_combinatorics(self):
        w = [x.pos[1] for x in self.crossings]
        self.min_wire = min(w)
        self.max_wire = max(w)+1
        self.length = len(w)
        self.compute_partial_perms()
        for i in range(len(self.crossings)):
            if self.crossings[i].dragging:
                dragging = True
                for wire in self.wires:
                    wire.active = False
                for x in self.crossings:
                    x.active = False
                return     
            if self.crossings[i].animate_my_wire:
                #print "Crossing %d was pushed by user" % i
                self.find_wires(self.crossings[i])
                self.bumped_crossing = i
                self.bump_direction = self.crossings[i].bump_direction
                self.other_crossing = None
                self.crossings[i].animate_my_wire = False
                self.crossings[i].bump_direction = None
            elif self.crossings[i].bump_direction != None:
                self.crossings[i].bump_direction = None
        if self.is_reduced():
            for wire in self.wires:
                wire.active = False
            self.bump_direction = None
            self.bumped_crossing = None
            self.other_crossing = None
        self.update_bump_constraints()

    def click(self, event):
        pass
        #print "Clicky!"

    def render(self):
        self.update_combinatorics()
        boxes=[]
        for i in range(self.min_wire, self.max_wire + 1):
            pi = self.perm()
            boxes.append(self.draw.text(str(i), (-1.5, i-0.5), self.color))
            boxes.append(self.draw.text(str(pi[i]), (self.length + 0.5, i-0.5), self.color))

            self.draw.line(self.color, (-1, i-0.5), (self.length, i-0.5), self.line_width)
        if self.is_reduced():
            self.draw.text("Reduced", (self.length/2, self.max_wire+1.0), self.color)
        else:
            self.draw.text("Not Reduced", (self.length/2, self.max_wire+1.0), Color.red)
        if self.bump_direction == 1:
            self.draw.text("Bumping Down", (self.length/2, self.max_wire+2.0), self.color)
        elif self.bump_direction == -1:
            self.draw.text("Bumping Up", (self.length/2, self.max_wire+2.0), self.color)
        for y in self.descents():
            #self.draw.circle(self.arrow_color, pos, 0.5)]
            
            width  = 0.1
            height = 0.15
            x = self.length+0.5
            inequality_sign = [(x-width, y-height), (x, y+height), (x+width, y-height)]
            self.draw.lines(self.color, False, inequality_sign, 3)
        return boxes

    def things_to_permute(self):
        return range(self.min_wire, self.max_wire+1)

    def perm(self):
        things_to_permute = self.things_to_permute()
        pi = dict(zip(things_to_permute, things_to_permute))
        for x in self.crossings:
            pi = x.transpose(pi)
        return pi

    def inv_perm(self, pi):
        tau = {}
        for x in pi:
            tau[pi[x]] = x
        return tau


    def compute_partial_perms(self):
        things_to_permute = self.things_to_permute()
        pi = dict(zip(things_to_permute, things_to_permute))
        self.partial_perms = [self.inv_perm(pi)]
        for x in self.crossings:
            pi = x.transpose(pi)
            self.partial_perms.append(self.inv_perm(pi))

# figure out the y coordinate of a crossing based on the y coordinates
# of its wires.  This has to happen differently in type B, hence it 
# gets its own method despite being trivial.
    def crossing_pos_from_heights(self, p1, p2):
        return min(p1, p2)


# make sure to compute_partial_perms first
    def find_wires(self, crossing):
        (x, y) = crossing.pos
        (pi, tau) = (self.inv_perm(self.partial_perms[x]), self.inv_perm(self.partial_perms[x+1]))
        points_moved = [pi[t] for t in self.things_to_permute() if pi[t] != tau[t]]
        node_lists = []
        valid_wires = []
        #print "Points_moved:", points_moved
        for p in points_moved:
            nodes = []
            for i in range(len(self.partial_perms)):
                pi = self.partial_perms[i]
                nodes.append((i, pi[p]))
            if abs(nodes[crossing.pos[0]][1] - crossing.pos[1]) <= 1:
                node_lists.append(self.fix_nodes_for_rendering(nodes))           
                valid_wires.append(nodes)
# find all places where wires cross
        self.wires_cross_at = []
        #print "Valid wires: ", valid_wires
        for i in range(1, len(valid_wires[0])):
            pre1 = valid_wires[0][i-1][1]
            pre2 = valid_wires[1][i-1][1]
            post1 = valid_wires[0][i][1]
            post2 = valid_wires[1][i][1]
            height_diff_pre = pre2 - pre1
            height_diff_post = post2 - post1
            if height_diff_pre == -height_diff_post:
                yi = self.crossing_pos_from_heights(pre1, pre2)
                self.wires_cross_at.append((i-1, yi))
        for (wire, nodes) in zip(self.wires, node_lists):
            wire.nodes = nodes
            #print crossing.pos
            wire.start_x = x
            wire.run_animation = True
        #print "Wires cross at:",  self.wires_cross_at

    def fix_nodes_for_rendering(self, nodes):
        shifted_nodes = [(x-0.5, y-0.5) for (x,y) in nodes]
        (first, last) = (shifted_nodes[0], shifted_nodes[-1])
        shifted_nodes.insert(0, (first[0]-0.5, first[1]))
        shifted_nodes.append((last[0]+0.5, last[1]))
        return shifted_nodes

    def inversions(self):
        result = 0
        pi = self.perm()

        for i in pi:
            for j in pi:
                if i<j and pi[i] > pi[j]:
                    result += 1
        return result

    def is_reduced(self):
        return self.inversions() == len(self)

    def is_vmarked_nearly_reduced(self, i):
        c = self.crossings.pop(i)
        result = self.is_reduced()
        self.crossings.insert(i, c)
        return result

    def descents(self):
        descents = []
        pi = self.perm()
        things_to_permute = sorted(pi.keys())
        for i in things_to_permute[1:]:
            if pi[i-1] > pi[i]:
                descents.append(i-1)
        return descents

